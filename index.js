require('dotenv').config();
const { SERVER_PORT } = process.env;
const app = require('./src');

// starting the server
app.listen(SERVER_PORT, () => console.log(`🚀 Server running on port ${SERVER_PORT}!`));
