'use strict';

const database = require('../database/database');

async function executeQuery(sql, values) {
  const pool = await database.getPool();
  return new Promise((resolve, reject) => {
    const query = pool.execute(sql, values);
    query
      .then((data) => {
        resolve(data[0]);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

async function valueExists(sql, values) {
  const pool = await database.getPool();
  return new Promise((resolve, reject) => {
    const query = pool.execute(sql, values);
    query
      .then((data) => {
        resolve(data[0].length > 0);
        reject(new Error('ESE VALOR NO EXISTE'));
      })
      .catch((error) => {
        reject(error);
      });
  });
}

module.exports = { executeQuery, valueExists };
