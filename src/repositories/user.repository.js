'use strict';

const repositoryManager = require('./repository.manager');
const { tableNames, columnBuilder } = require('../helpers');

async function registerUser(user) {
  const insertUserRergister = `INSERT INTO ${tableNames.USER} (id, email, password) VALUES (?,?,?)`;
  console.log({ user });
  const values = [user.id, user.email, user.password];
  return await repositoryManager.executeQuery(insertUserRergister, values);
}

async function valueExists(value, tableValue) {
  const selectUserEmail = `SELECT ${tableValue} FROM  ${tableNames.USER} WHERE ${tableValue} = ? LIMIT 1`;
  const values = [value];
  return await repositoryManager.valueExists(selectUserEmail, values);
}

async function findByEmail(email) {
  const selectUserEmail = `SELECT * FROM  ${tableNames.USER} WHERE email = ? LIMIT 1`;
  const values = [email];
  return await repositoryManager.executeQuery(selectUserEmail, values);
}
async function findById(id) {
  const selectUserId = `
  SELECT
  u.id, u.fullname, u.dni,DATE_FORMAT(u.borndate, "%d/%m/%Y") as borndate, u.email, u.picture, u.bio,
  ud.street , ud.city,ud.country, ud.zipcode,AVG(ur.rating) as userRating
  FROM  ${tableNames.USER} u
  LEFT JOIN user_addresses ud ON u.id = ud.id_user
  LEFT JOIN user_rating ur ON u.id = ur.id_user_voted
  WHERE u.id = ?`;
  const values = [id];
  return await repositoryManager.executeQuery(selectUserId, values);
}

async function getPublicationUser(id) {
  const selectUserId = `
  SELECT
  p.id, p.area,p.rooms,p.bathrooms,p.garage,p.elevator,p.furnished,p.publication_type,p.deposit,p.price,p.availability_date,p.disabled,
  pr.rating as publicationRating
  FROM ${tableNames.USER} u LEFT JOIN ${tableNames.PUBLICATION} p ON u.id = p.id_user
  LEFT JOIN ${tableNames.PUBLICATION_RATINGS}  pr ON pr.id_publication = p.id  where u.id = ? GROUP BY p.id;`;
  const values = [id];
  return await repositoryManager.executeQuery(selectUserId, values);
}

async function getPublicationFavoriteUser(id) {
  const selectUserId = `
  SELECT
  p.id, p.area,p.rooms,p.bathrooms,p.garage,p.elevator,p.furnished,p.publication_type,p.deposit,p.price,p.availability_date,p.disabled,
  AVG(pr.rating) as publicationRating
  FROM ${tableNames.USER} u
  LEFT JOIN ${tableNames.USER_PUBLICATIONS_FAVORITES} upf ON u.id = upf.id_user
  LEFT JOIN ${tableNames.PUBLICATION} p ON  upf.id_publication = p.id
  LEFT JOIN ${tableNames.PUBLICATION_RATINGS} pr ON pr.id_publication = p.id where u.id = ? GROUP BY p.id`;
  const values = [id];
  return await repositoryManager.executeQuery(selectUserId, values);
}

async function verifyUser(id) {
  const selectUserId = `UPDATE ${tableNames.USER} SET verified = 1 WHERE id = ?`;
  const values = [id];
  return await repositoryManager.executeQuery(selectUserId, values);
}

async function updateUser(user, id) {
  const { columnSet, values } = await columnBuilder(user);
  const sql = `UPDATE ${tableNames.USER} SET ${columnSet} WHERE id = ?`;
  return await repositoryManager.executeQuery(sql, [...values, id]);
}
module.exports = {
  findByEmail,
  findById,
  getPublicationFavoriteUser,
  getPublicationUser,
  registerUser,
  updateUser,
  valueExists,
  verifyUser,
};
