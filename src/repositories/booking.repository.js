'use strict';

const repositoryManager = require('./repository.manager');
const { queryBuilder, tableNames, columnBuilder } = require('../helpers');

async function insertBooking(booking) {
  const { query, values } = await queryBuilder(tableNames.BOOKING, booking);
  return await repositoryManager.executeQuery(query, values);
}

async function updateBooking(booking, id) {
  const { columnSet, values } = await columnBuilder(booking);
  const sql = `UPDATE ${tableNames.BOOKING} SET ${columnSet} WHERE id = ?`;
  return await repositoryManager.executeQuery(sql, [...values, id]);
}

async function findBookingById(id) {
  const query = `SELECT * FROM ${tableNames.BOOKING} WHERE id = ? LIMIT 1`;
  return await repositoryManager.executeQuery(query, [id]);
}

async function deleteBooking(id) {
  const query = `DELETE FROM ${tableNames.BOOKING} WHERE id = ?`;
  return await repositoryManager.executeQuery(query, [id]);
}

module.exports = {
  deleteBooking,
  findBookingById,
  insertBooking,
  updateBooking,
};
