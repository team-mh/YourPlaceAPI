const compression = require('compression');
const express = require('express');
const cors = require('cors');

const { httpStatus, ResponseError } = require('./helpers');
const errorMiddleware = require('./middleware/error.middleware');

const userRouter = require('./api/user/routes/user.routes');
const publicationsRouter = require('./api/publication/routes/publication.routes');
const visitsRouter = require('./api/visit/routes/visit.routes');
const bookingsRouter = require('./api/booking/routes/booking.routes');
const messageRouter = require('./api/messages/routes/message.routes');
const transactionsRouter = require('./api/transactions/routes/transaction.routes');

const app = express();

app.use(compression());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(cors());
app.options('*', cors());

app.use('/api/v1/users', userRouter);
app.use('/api/v1/publications', publicationsRouter);
app.use('/api/v1/visits', visitsRouter);
app.use('/api/v1/bookings', bookingsRouter);
app.use('/api/v1/messages', messageRouter);
app.use('/api/v1/transactions', transactionsRouter);

// Endpoint Not Found
app.all('*', (req, res, next) => {
  const err = new ResponseError(httpStatus.NOT_FOUND, 'Endpoint Not Found');
  next(err);
});

// Error middleware
app.use(errorMiddleware);

module.exports = app;
