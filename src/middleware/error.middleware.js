const httpStatus = require('../helpers/httpStatus.helper');
function errorMiddleware(error, req, res, next) {
  let { status = httpStatus.INTERNAL_SERVER_ERROR, message, data } = error;

  console.log(`[Error] ${error}`);

  // If status code is 500 - change the message to Internal server error
  message =
    status === httpStatus.INTERNAL_SERVER_ERROR || !message ? 'Internal server error' : message;

  error = {
    type: 'error',
    status,
    message,
    ...(data && data),
  };

  res.status(status).send(error);
}

module.exports = errorMiddleware;
