'use strict';

const Joi = require('joi');

const ratingSchema = Joi.object({
  rating: Joi.number().positive().min(0).max(5).required(),
  comment: Joi.string().max(200).required(),
  id_publication: Joi.string().required(),
  id_user_voter: Joi.string().required(),
});

const validateRating = async (rating) => {
  return await ratingSchema.validateAsync(rating);
};

module.exports = validateRating;
