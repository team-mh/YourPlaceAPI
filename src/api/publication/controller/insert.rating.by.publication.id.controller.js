'use strict';

const { httpStatus } = require('../../../helpers');
const publicationServices = require('../services');

async function insertRatingByPublicationId(request, response) {
  const { id } = request.params;
  const { rating, comment } = request.body;
  const { id: idUser } = request.user;
  await publicationServices.insertRatingByPublicationId(id, rating, comment, idUser);
  return response.status(httpStatus.OK).send('RATED');
}

module.exports = insertRatingByPublicationId;
