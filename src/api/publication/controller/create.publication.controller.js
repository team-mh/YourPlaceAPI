'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const publicationServices = require('../services/');

async function createPublication(req, res) {
  const pictures = req.body.pictures;
  const publicationAddress = req.body.publication_address;
  const publication = req.body.publication;
  const { id } = req.user;
  try {
    await publicationServices.createPublication(publication, publicationAddress, pictures, id);
    res.status(httpStatus.CREATED).send('CREATED');
  } catch (error) {
    res
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = createPublication;
