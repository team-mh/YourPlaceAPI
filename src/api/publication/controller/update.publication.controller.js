'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const publicationServices = require('../services/');

async function updatePublication(request, response) {
  const { publication, publication_address: publicationAddress } = request.body;
  try {
    await publicationServices.updatePublication(publication, publicationAddress, request.user.id);
    response.status(httpStatus.OK).send('UPDATED');
  } catch (error) {
    response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = updatePublication;
