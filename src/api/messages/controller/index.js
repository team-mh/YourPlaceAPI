module.exports = {
  newMessage: require('./post.new.message.controller'),
  getMyTalks: require('./get.MyTalks.message.controller'),
  getOurTalk: require('./get.OurTalk.message.controller'),
};
