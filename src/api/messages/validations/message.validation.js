'use strict';

const Joi = require('joi');

const messageSchema = Joi.object({
  message: Joi.string().max(200).required(),
  idUserReceiver: Joi.string().required(),
});

const messageValidation = async (message) => {
  return await messageSchema.validateAsync(message);
};

module.exports = { messageValidation };
