const messageRouter = require('express').Router();
const MessageController = require('../controller');
const { auth } = require('../../user/middleware/auth.user');

messageRouter
  .route('/')
  .all(auth)
  .post(async (request, response) => await MessageController.newMessage(request, response))
  .get(async (request, response) => await MessageController.getMyTalks(request, response));

messageRouter
  .route('/:id')
  .all(auth)
  .get(async (request, response) => await MessageController.getOurTalk(request, response));

module.exports = messageRouter;
