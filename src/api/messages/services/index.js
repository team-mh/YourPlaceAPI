module.exports = {
  insertMessage: require('./insert.message.service'),
  findMyTalks: require('./findMyTalks.message.service'),
  findOurTalk: require('./findOurTalk.message.service'),
};
