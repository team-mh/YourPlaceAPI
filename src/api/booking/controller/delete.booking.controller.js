'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const bookingServices = require('../services');

async function deleteBooking(request, response) {
  try {
    const { id: idBooking } = request.params;
    const { id: idUser } = request.user;
    await bookingServices.deleteBooking(idBooking, idUser);
    return response.status(httpStatus.OK).send('BOOKING DELETED');
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = deleteBooking;
