'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const bookingServices = require('../services');

async function getBookingById(request, response) {
  try {
    const { id: idBooking } = request.params;
    const booking = await bookingServices.getBookingById(idBooking);
    return response.status(httpStatus.OK).send(booking);
  } catch (error) {
    return response
      .status(httpStatus.NO_CONTENT)
      .send(new ResponseError(httpStatus.NO_CONTENT, error, error.message));
  }
}

module.exports = getBookingById;
