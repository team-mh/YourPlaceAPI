'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const bookingServices = require('../services/');

async function insertBooking(request, response) {
  try {
    const { start_date: startDate, months, id_publication: idPublication } = request.body;
    const { id: idUser } = request.user;
    await bookingServices.insertBooking(startDate, months, idPublication, idUser);
    return response.status(httpStatus.OK).send('INSERTED BOOKING');
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = insertBooking;
