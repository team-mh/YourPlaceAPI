'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const bookingServices = require('../services/');

async function updateBooking(request, response) {
  try {
    const { start_date: startDate, months, id: idBooking } = request.body;
    const { id: idUser } = request.user;
    console.log(request.body);
    await bookingServices.updateBooking(startDate, months, idBooking, idUser);
    return response.status(httpStatus.OK).send('BOOKING UPDATED');
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = updateBooking;
