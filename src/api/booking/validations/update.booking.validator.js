'use strict';

const Joi = require('joi');

const updateBookingSchema = Joi.object({
  start_date: Joi.date().min('now').required(),
  end_date: Joi.date(),
});

const updateBookingValidation = async (booking) => {
  return await updateBookingSchema.validateAsync(booking);
};

module.exports = updateBookingValidation;
