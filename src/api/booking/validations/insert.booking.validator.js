'use strict';

const Joi = require('joi');

const insertBookingSchema = Joi.object({
  id: Joi.string().required(),
  start_date: Joi.date().min('now').required(),
  end_date: Joi.date(),
  id_user_payer: Joi.string().required(),
  id_publication: Joi.string().required(),
});

const insertBookingValidation = async (booking) => {
  return await insertBookingSchema.validateAsync(booking);
};

module.exports = insertBookingValidation;
