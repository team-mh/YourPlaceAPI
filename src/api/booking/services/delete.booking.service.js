'use strict';

const bookingRepository = require('../../../repositories/booking.repository');

async function deleteBooking(idBooking, idUser) {
  const [foundBooking] = await bookingRepository.findBookingById(idBooking);
  console.log(foundBooking);
  if (foundBooking) {
    if (foundBooking.id_user_payer === idUser) {
      // return await bookingRepository.deleteBooking(idBooking);
      return;
    }
    throw new Error('NO TIENES PERMISOS PARA BORRAR ESA RESERVA');
  }

  throw new Error('NO SE HA ENCONTRADO ESA RESERVA');
}

module.exports = deleteBooking;
