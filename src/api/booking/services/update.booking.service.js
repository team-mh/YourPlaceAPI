'use strict';

const { add, format } = require('date-fns');
const { updateBookingValidator } = require('../validations');
const bookingRepository = require('../../../repositories/booking.repository');

async function updateBooking(startDate, months, idBooking, idUser) {
  const [foundBooking] = await bookingRepository.findBookingById(idBooking);
  if (foundBooking) {
    if (foundBooking.id_user_payer === idUser) {
      const date = add(new Date(startDate), { months: months });
      const endDate = format(date, 'yyyy/MM/dd');
      const booking = {
        start_date: startDate,
        end_date: endDate,
      };
      await updateBookingValidator(booking);
      return await bookingRepository.updateBooking(booking, idBooking);
    }
    throw new Error('NO TIENES PERMISOS PARA EDITAR ESA RESERVA');
  }
  throw new Error('NO SE HA ENCONTRADO ESA RESERVA');
}

module.exports = updateBooking;
