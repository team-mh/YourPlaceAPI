'use strict';

const bookingRepository = require('../../../repositories/booking.repository');

async function getBookingById(idBooking) {
  if (idBooking) {
    const [booking] = await bookingRepository.findBookingById(idBooking);
    if (booking) {
      return booking;
    }
    throw new Error('NO SE HA ENCONTRADO RESERVA');
  }
  throw new Error('NO ES UN ID VALIDO');
}

module.exports = getBookingById;
