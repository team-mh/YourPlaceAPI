'use strict';

const bookingRepository = require('../../../repositories/booking.repository');
const { insertBookingValidator } = require('../validations');
const { idChecker, tableNames } = require('../../../helpers');
const { add, format } = require('date-fns');

async function insertBooking(startDate, months, idPublication, idUser) {
  const id = await idChecker(tableNames.BOOKING);
  const date = add(new Date(startDate), { months: months });
  const endDate = format(date, 'yyyy/MM/dd');
  const booking = {
    id: id,
    start_date: startDate,
    end_date: endDate,
    id_user_payer: idUser,
    id_publication: idPublication,
  };
  await insertBookingValidator(booking);
  return await bookingRepository.insertBooking(booking);
}

module.exports = insertBooking;
