const userRouter = require('express').Router();
const userController = require('../controllers');
const cache = require('../../../middleware/cache.middleware');
const { cacheTime } = require('../../../helpers');
const { auth } = require('../middleware/auth.user');

userRouter.post(
  '/register',
  async (request, response) => await userController.registerUser(request, response)
);

userRouter.post('/login', async (request, response) => {
  console.log(request.body);
  return await userController.loginUser(request, response);
});

userRouter.post('/logout', async (request, response) => {});

userRouter.get(
  '/verify/:id/:secretCode',
  auth,
  async (request, response) => await userController.verifyUser(request, response)
);

userRouter.put(
  '/',
  auth,
  async (request, response) => await userController.updateUser(request, response)
);

userRouter.get(
  '/',
  auth,
  cache(cacheTime.ONE_HOUR),
  async (request, response) => await userController.getMeUser(request, response)
);

userRouter.get(
  '/:id',
  cache(cacheTime.ONE_HOUR),
  async (request, response) => await userController.getUser(request, response)
);

userRouter
  .route('/ratings')
  .all(auth)
  .post(async (request, response) => await userController.postRating(request, response))
  .put(async (request, response) => await userController.putRating(request, response));
module.exports = userRouter;
