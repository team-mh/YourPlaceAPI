'use strict';

const Joi = require('joi');

const loginUserSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

const loginUserValidation = async (user) => {
  return await loginUserSchema.validateAsync(user);
};

module.exports = loginUserValidation;
