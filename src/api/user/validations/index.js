module.exports = {
  loginUserValidation: require('./userLogin.validator'),
  userValidation: require('./user.validation'),
  addressValidation: require('./address.validation'),
  ratingValidation: require('./rating.validator'),
};
