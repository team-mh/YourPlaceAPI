'use strict';

const Joi = require('joi');

const addressSchema = Joi.object({
  street: Joi.string().max(200).required(),
  city: Joi.string().max(50).required(),
  country: Joi.string().max(50).required(),
  zipcode: Joi.number().positive().required(),
});

const addressValidation = async (user) => {
  return await addressSchema.validateAsync(user);
};

module.exports = addressValidation;
