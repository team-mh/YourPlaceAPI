'use strict';

const Joi = require('joi');

const dniPatternRexp = '^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$';

const userSchema = Joi.object({
  fullname: Joi.string(),
  dni: Joi.string().pattern(new RegExp(dniPatternRexp, 'i')),
  borndate: Joi.date(),
  password: Joi.string(),
  email: Joi.string().email().required(),
  picture: Joi.string(),
  bio: Joi.string().max(180),
  telephone: Joi.string(),
});

const userValidation = async (user) => {
  return await userSchema.validateAsync(user);
};

module.exports = userValidation;
