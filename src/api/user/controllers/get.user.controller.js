const userServices = require('../services');

const { httpStatus, ResponseError } = require('../../../helpers');

async function getMeUser(request, response) {
  const { id } = request.params;
  try {
    const getUser = await userServices.getUser(id);
    response.status(httpStatus.OK).send(getUser);
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = getMeUser;
