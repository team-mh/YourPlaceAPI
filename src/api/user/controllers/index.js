module.exports = {
  getMeUser: require('./getme.user.controller'),
  getUser: require('./get.user.controller'),
  loginUser: require('./login.user.controller'),
  postRating: require('./postRating.user.controller'),
  putRating: require('./putRating.user.controller'),
  putUser: require('./put.user.controller'),
  registerUser: require('./register.user.controller'),
  verifyUser: require('./verify.user.controller'),
};
