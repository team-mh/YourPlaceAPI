'use strict';
const userServices = require('../services');

const { httpStatus, ResponseError } = require('../../../helpers');

async function putUser(request, response) {
  const { body, user: token } = request;
  const { id } = token;
  const { user, address } = body;
  try {
    const setUser = await userServices.updateUser(user, address, id);
    response.status(httpStatus.OK).send(setUser);
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = putUser;
