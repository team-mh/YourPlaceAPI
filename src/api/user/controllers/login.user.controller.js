'use strict';

const jwt = require('jsonwebtoken');
const userServices = require('../services');

const { httpStatus, ResponseError } = require('../../../helpers');

async function loginUser(request, response) {
  const { email, password } = request.body;
  const user = { email: email, password: password };
  try {
    const userLogged = await userServices.loginUser(user);
    const token = jwt.sign(
      { id: userLogged.id, verified: userLogged.verified },
      process.env.TOKEN_SECRET,
      {
        expiresIn: '60m',
      }
    );
    response
      .header('Authorization', `Bearer ${token}`)
      .status(httpStatus.OK)
      .send({ response: 'Logged In!', authorization: token });
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = loginUser;
