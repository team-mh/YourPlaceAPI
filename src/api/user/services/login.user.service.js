'use strict';

const bcrypt = require('bcryptjs');
const schemaValidation = require('../validations');
const userRepository = require('../../../repositories/user.repository');
const { tableValue } = require('../../../helpers');

async function loginUser({ email, password }) {
  const userDestructuring = { email: email, password: password };
  await schemaValidation.loginUserValidation(userDestructuring);
  await userRepository.valueExists(email, tableValue.EMAIL);

  const [userdb] = await userRepository.findByEmail(email);
  return await bcrypt.compare(password, userdb.password).then((res) => {
    if (res === true) {
      return userdb;
    }
    throw new Error('MISSMATCH EMAIL OR PASSWORD');
  });
}

module.exports = loginUser;
