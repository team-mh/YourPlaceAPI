'use strict';
const { ResponseError, httpStatus } = require('../../../helpers');
const userRatingRepository = require('../../../repositories/userRating.respository');
const schemaValidation = require('../validations');

async function insertRating(ratingBody, idUser) {
  await schemaValidation.ratingValidation(ratingBody);

  const { rating, comment, idUserVoted } = ratingBody;
  const ratingEntity = {
    rating: rating,
    comment: comment,
    id_user_voted: idUserVoted,
    id_user_voter: idUser,
  };

  const ratingExist = await userRatingRepository.valueExists(idUser, idUserVoted);

  if (ratingExist) {
    throw new ResponseError(httpStatus.BAD_REQUEST, `It already exists`);
  }

  return await userRatingRepository.insertUserRating(ratingEntity);
}

module.exports = insertRating;
