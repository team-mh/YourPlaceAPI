module.exports = {
  loginUser: require('./login.user.service'),
  registerUser: require('./register.user.service'),
  sendEmail: require('./sendEmail.user.service'),
  updateUser: require('./update.user.service'),
  verifyUser: require('./verify.user.service'),
  getUser: require('./get.user.service'),
  updateRating: require('./updateRating.user.service'),
  insertRating: require('./insertRating.user.service'),
};
