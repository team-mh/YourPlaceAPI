'use strict';
const { ResponseError, httpStatus } = require('../../../helpers');
const userRatingRepository = require('../../../repositories/userRating.respository');
const schemaValidation = require('../validations');

async function updateRating(ratingBody, idUser) {
  await schemaValidation.ratingValidation(ratingBody);
  const { rating, comment, idUserVoted } = ratingBody;
  const ratingEntity = {
    rating: rating,
    comment: comment,
    id_user_voted: idUserVoted,
  };
  const ratingExist = await userRatingRepository.valueExists(idUser, idUserVoted);

  if (ratingExist) {
    return await userRatingRepository.updateUserRating(ratingEntity, idUser);
  }

  throw new ResponseError(httpStatus.BAD_REQUEST, `it was not updated`);
}

module.exports = updateRating;
