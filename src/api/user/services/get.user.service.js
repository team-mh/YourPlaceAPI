const userRepository = require('../../../repositories/user.repository');
const publicationRepository = require('../../../repositories/publication.repository');
async function getUser(id) {
  const [user] = await userRepository.findById(id);
  let [...publicationsUser] = await userRepository.getPublicationUser(id);
  let [...publicationsFavoritesUser] = await userRepository.getPublicationFavoriteUser(id);

  publicationsUser = await publicationsUser.map(async (publication) => {
    const pics = await publicationRepository.findAllPicturesByPublicationId(publication.id);
    const pictures = pics.map((pic) => pic.url);
    return { ...publication, pictures: pictures };
  });

  publicationsFavoritesUser = await publicationsFavoritesUser.map(async (publication) => {
    const pics = await publicationRepository.findAllPicturesByPublicationId(publication.id);
    const pictures = pics.map((pic) => pic.url);
    return { ...publication, pictures: pictures };
  });

  publicationsUser = await Promise.all(publicationsUser).then((completed) => completed);
  publicationsFavoritesUser = await Promise.all(publicationsFavoritesUser).then(
    (completed) => completed
  );

  return { user, publicationsUser, publicationsFavoritesUser };
}

module.exports = getUser;
