'use strict';
const jwt = require('jsonwebtoken');

const { httpStatus, ResponseError } = require('../../../helpers');

function auth(request, response, next) {
  try {
    const { authorization } = request.headers;
    if (!authorization || !authorization.startsWith('Bearer')) {
      return response
        .status(httpStatus.FORBIDDEN)
        .send(new ResponseError(httpStatus.FORBIDDEN, 'Authorization required'));
    }

    const token = authorization.split(' ')[1];
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    request.user = verified;
    next();
  } catch (error) {
    response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, 'Invalid Token'));
  }
}

module.exports = { auth };
