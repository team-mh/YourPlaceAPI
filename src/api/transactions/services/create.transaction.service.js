'use strict';

const { idChecker, tableNames, ResponseError, httpStatus } = require('../../../helpers');
const { insertTransactionValidator } = require('../validations/');
const transactionRepository = require('../../../repositories/transaction.repository');
const bookingRepository = require('../../../repositories/booking.repository');
const publicationRepository = require('../../../repositories/publication.repository');
const { fromUnixTime } = require('date-fns');
const CURRENCY_CENTS = 100;

async function createTransaction(timestamp, amount, success, idBooking, idUser) {
  const idTransaction = await idChecker(tableNames.TRANSACTIONS);
  const [booking] = await bookingRepository.findBookingById(idBooking);
  const [publication] = await publicationRepository.findPublicationById(booking.id_publication);
  if (publication.id === booking.id_publication) {
    if (booking.id_user_payer === idUser) {
      const transaction = {
        id: idTransaction,
        timestamp: fromUnixTime(timestamp),
        amount: amount / CURRENCY_CENTS,
        success: success,
        id_booking: idBooking,
      };
      await insertTransactionValidator(transaction);
      await transactionRepository.createTransaction(transaction);
      if (success) {
        return await publicationRepository.updatePublication({ disabled: success }, publication.id);
      }
      throw new ResponseError(httpStatus.BAD_REQUEST, 'NO SE HA PODIDO COMPLETAR LA TRANSACCION');
    }
    throw new ResponseError(httpStatus.FORBIDDEN, 'NO TIENES PERMISOS PARA PAGAR LA RESERVA');
  }
  throw new ResponseError(
    httpStatus.UNAUTHORIZED,
    'NO EXISTE RELACION ENTRE LA RESERVA Y LA PUBLICACION'
  );
}

module.exports = createTransaction;
