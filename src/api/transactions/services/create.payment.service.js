'use strict';

const { STRIPE_API_KEY } = process.env;
const stripe = require('stripe')(STRIPE_API_KEY);
const calculatePayment = require('./calculate.payment.service');

async function createPayment(idBooking, idUser) {
  const result = await calculatePayment(idBooking, idUser);
  if (result) {
    return await stripe.paymentIntents.create({
      amount: result,
      currency: 'eur',
    });
  }
  throw new Error('NO SE HA PODIDO CALCULAR EL TOTAL DE LA RESERVA');
}

module.exports = createPayment;
