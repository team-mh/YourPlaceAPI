'use strict';

const Joi = require('joi');

const insertTransactionSchema = Joi.object({
  id: Joi.string().required(),
  timestamp: Joi.date().required(),
  amount: Joi.number().required(),
  success: Joi.boolean().required(),
  id_booking: Joi.string().required(),
});

const insertTransactionValidation = async (transaction) => {
  return await insertTransactionSchema.validateAsync(transaction);
};

module.exports = insertTransactionValidation;
