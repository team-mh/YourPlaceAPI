'use strict';

const visitValidator = require('../validations');
const getVisitById = require('./get.visit.by.id.service');
const visitRepository = require('../../../repositories/visit.repository');

async function updateVisit(id, visitDate, visitHour, idUser) {
  const foundVisit = await getVisitById(id);

  if (foundVisit) {
    if (idUser === foundVisit.id_user_visitant) {
      const visit = {
        visit_date: visitDate,
        visit_hour: visitHour,
        acepted: false,
      };
      await visitValidator.validateUpdateVisit(visit);
      return await visitRepository.updateVisit(visit, id);
    }
    throw new Error('NO TIENES PERMISO PARA EDITAR ESA VISITA');
  }
  throw new Error('NO SE ENCUENTRA ESA VISITA');
}
module.exports = updateVisit;
