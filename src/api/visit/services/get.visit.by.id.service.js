'use strict';

const visitRepository = require('../../../repositories/visit.repository');

async function getVisitById(id) {
  if (id) {
    const [visit] = await visitRepository.getVisityById(id);
    if (visit) {
      return visit;
    }
    throw new Error('NO SE HA PODIDO ENCONTRAR LA VISITA');
  }
  throw new Error('NO ES UN ID VALIDO');
}

module.exports = getVisitById;
