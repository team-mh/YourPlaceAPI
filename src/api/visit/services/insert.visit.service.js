'use strict';

const visitValidator = require('../validations');
const { idChecker, tableNames } = require('../../../helpers');
const visitRepository = require('../../../repositories/visit.repository');
const publicationRepository = require('../../../repositories/publication.repository');

async function insertVisit(visitDate, visitHour, idPublication, idUser) {
  const visitId = await idChecker(tableNames.VISIT);
  const visit = {
    id: visitId,
    visit_date: visitDate,
    visit_hour: visitHour,
    id_publication: idPublication,
    id_user_visitant: idUser,
  };
  await visitValidator.validateInsertVisit(visit);

  const publication = await publicationRepository.existsPublication(idPublication);
  if (publication) {
    return await visitRepository.insertVisit(visit);
  }

  throw new Error('NO SE HA ENCONTRADO UNA PUBLICACION PARA ESA VISITA');
}

module.exports = insertVisit;
