'use strict';

const visitRepository = require('../../../repositories/visit.repository');
const getVisitById = require('./get.visit.by.id.service');

async function deleteVisit(idVisit, idUser) {
  if (idVisit) {
    const visitFound = await getVisitById(idVisit);
    if (visitFound.id_user_visitant === idUser) {
      return await visitRepository.deleteVisit(idVisit);
    }
    throw new Error('NO TIENES PERMISO PARA BORRAR LA VISITA');
  }
  throw new Error('ESE ID NO ES VALIDO');
}

module.exports = deleteVisit;
