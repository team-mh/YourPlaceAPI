'use strict';

const Joi = require('joi');

const visitRegex = '^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$';

const insertVisitSchema = Joi.object({
  id: Joi.string().required(),
  visit_date: Joi.date().min('now').required(),
  visit_hour: Joi.string().pattern(new RegExp(visitRegex, 'm')).required(),
  acepted: Joi.boolean(),
  id_publication: Joi.string().required(),
  id_user_visitant: Joi.string().required(),
});

const visitValidation = async (user) => {
  return await insertVisitSchema.validateAsync(user);
};

module.exports = visitValidation;
