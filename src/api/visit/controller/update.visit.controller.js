'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const visitServices = require('../services');

async function updateVisit(request, response) {
  try {
    const { id, visit_date: visitDate, visit_hour: visitHour } = request.body;
    const { id: idUser } = request.user;

    await visitServices.updateVisit(id, visitDate, visitHour, idUser);

    return response.status(httpStatus.OK).send('VISIT UPDATED');
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = updateVisit;
