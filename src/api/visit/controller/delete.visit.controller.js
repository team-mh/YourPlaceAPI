'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const visitServices = require('../services');

async function deleteVisit(request, response) {
  try {
    const { id } = request.params;
    const { id: idUser } = request.user;
    await visitServices.deleteVisit(id, idUser);
    return response.status(httpStatus.OK).send('VISIT DELETED');
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = deleteVisit;
