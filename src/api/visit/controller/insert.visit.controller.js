'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const visitServices = require('../services');

async function insertVisit(request, response) {
  try {
    const {
      visit_date: visitDate,
      visit_hour: visitHour,
      id_publication: idPublication,
    } = request.body;
    const idUser = request.user.id;

    await visitServices.insertVisit(visitDate, visitHour, idPublication, idUser);
    return response.status(httpStatus.OK).send('INSERTED VISIT');
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = insertVisit;
