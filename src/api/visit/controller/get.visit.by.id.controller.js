'use strict';

const { httpStatus, ResponseError } = require('../../../helpers');
const visitServices = require('../services');

async function getVisitById(request, response) {
  try {
    const { id } = request.params;
    const visit = await visitServices.getVisitById(id);

    return response.status(httpStatus.OK).send(visit);
  } catch (error) {
    return response
      .status(httpStatus.BAD_REQUEST)
      .send(new ResponseError(httpStatus.BAD_REQUEST, error, error.message));
  }
}

module.exports = getVisitById;
